const {resolve} = require('path')
const express = require('express')
const webpack = require('webpack')
const config = require('./webpack.config')({dev: true})

var port = 3000
var app = express()
var compiler = webpack(config)

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath
}))

app.use(require('webpack-hot-middleware')(compiler))

app.get('*', function (req, res) {
  console.log(req.originalUrl)
  let url = req.originalUrl === '/' ? 'index.html' : req.originalUrl.slice(1)
  //if (req.originalUrl.substring('/dcecffbcd86a64a1114f.hot-update.json'))
  let file = resolve(__dirname, '..', 'src', url)
  res.sendFile(file)
})

/* eslint-disable no-console */
app.listen(port, function onAppListening (err) {
  if (err) {
    console.error(err)
  } else {
    console.info('==> 🚧  Webpack development server listening on port %s', port)
  }
})
