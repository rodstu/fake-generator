const {resolve} = require('path')
const webpack = require('webpack')
const autoprefixer = require('autoprefixer')
const WebpackMd5Hash = require('webpack-md5-hash')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = env => {
  return {
    entry: getEntry(env.prod),
    output: getOutput(env.prod),
    devtool: env.prod ? 'hidden-source-map' : 'eval',
    context: resolve(__dirname, '..', 'src'),
    module: getModules(env.prod),
    plugins: getPlugins(env),
    resolve: {
      extensions: ['.js', '.scss', '.css']
    }
  }
}

// ======================================= HELPERS ==================================================

const getEntry = (prod) => {
  return !prod
    ? ['./app.js',
      'webpack-hot-middleware/client']
    : {app: [
      './app.js'
    ],
      vendor: [
        'classnames',
        'material-ui',
        'react',
        'react-dom',
        'react-easy-transition',
        'react-fa',
        'react-router',
        'react-tap-event-plugin'
      ]}
}

const getOutput = (prod) => {
  return prod
    ? {path: resolve(__dirname, '..', '_dist'),
      chunkFilename: '[name].[chunkhash:20].js',
      filename: '[name].[chunkhash].js',
      publicPath: ''
    }
    : {path: resolve(__dirname, '..', '_dev'),
      filename: 'app.js',
      publicPath: ''
    }
}

const cssLoaders = (prod) => {
  let localIdentName = prod
    ? '[hash:base64]'
    : '[path][name]_[local]_[hash:base64:5]'

  return prod
    ? ExtractTextPlugin.extract({
      fallbackLoader: 'style',
      loader: 'css?module&camelCase&localIdentName=' + localIdentName + '!postcss!sass'
    })
    : 'style!css?module&camelCase&localIdentName=' + localIdentName + '!postcss!sass'
}

const getPlugins = (env) => {
  console.log(env)
  if (env.prod) return getPRODplugins()
  if (env.dev) return getDEVplugins()
}

const getModules = (prod) => {
  return {
    loaders: [{
      test: /\.js$/,
      loader: 'babel',
      exclude: /node_modules/
    }, {
      test: /\.s?css?$/,
      loader: cssLoaders(prod)
    }, {
      test: /\.png$/,
      loader: 'url?limit=100000&mimetype=image/png'
    }, {
      test: /\.jpg$/,
      loader: 'url?limit=100000&mimetype=image/jpg'
    }, {
      test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
      loader: 'url-loader?limit=10000&mimetype=application/font-woff'
    }, {
      test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
      loader: 'file-loader'
    }]
  }
}

const getPRODplugins = () => [
  new WebpackMd5Hash(),
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify('production'),
    ENV: 'PRODUCTION'
  }),
  new webpack.optimize.DedupePlugin(),
  new webpack.optimize.OccurrenceOrderPlugin(),
  new webpack.optimize.CommonsChunkPlugin({
    name: 'vendor',
    minChunks: Infinity
  }),
  new ExtractTextPlugin('[name].[contenthash:20].css'),
  new HtmlWebpackPlugin({
    template: './index.html'
  }),
  new webpack.LoaderOptionsPlugin({
    minimize: true,
    debug: false,
    options: {
      postcss: [autoprefixer({ browsers: ['last 3 versions'] })]
    }
  }),
  new webpack.optimize.UglifyJsPlugin({
    beautify: false,
    comments: false,
    sourceMap: false,
    compressor: {
      screw_ie8: true,
      drop_console: true,
      drop_debugger: true,
      warnings: false
    }
  })
]

const getDEVplugins = () => [
  new webpack.LoaderOptionsPlugin({
    minimize: false,
    debug: true
  }),
  new webpack.DefinePlugin({
    ENV: 'DEVELOPMENT'
  }),
  new HtmlWebpackPlugin({
    template: './index.html'
  }),
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NoErrorsPlugin()
]
