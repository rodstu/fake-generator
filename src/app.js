import React, { Component } from 'react'
import { render } from 'react-dom'
import injectTapEventPlugin from 'react-tap-event-plugin'
import Router from 'react-router/lib/Router'
import Route from 'react-router/lib/Route'
import IndexRoute from 'react-router/lib/IndexRoute'
import hashHistory from 'react-router/lib/hashHistory'

import Container from './components/Container/Container'
import Index from './components/Home/Index'

class App extends Component {
  routeHandler (previousRoute, nextRoute) {
    let arr = []
    nextRoute.routes.map((r) => {
      arr.push(r.path)
    })
    return arr.join('')
  }

  render () {
    return (
      <Router history={hashHistory}>
        <Route
          path='/'
          component={Container}
          onChange={this.routeHandler}>
          <IndexRoute component={Index} />
          <Route path='numerico/cpf' getComponent={(location, callback) => {
            System.import('./components/Number/CPF/CPF').then((component) => callback(null, component.default || component))
          }} />
          <Route path='numerico/cnpj' getComponent={(location, callback) => {
            System.import('./components/Number/CNPJ/CNPJ').then((component) => callback(null, component.default || component))
          }} />
          <Route path='codigobarra/boleto' getComponent={(location, callback) => {
            System.import('./components/Barcode/Boleto/Boleto').then((component) => callback(null, component.default || component))
          }} />
          {/*<Route path='numerico/cnpj' components={{ content: CNPJ, context: CNPJContext }} />*/}
        </Route>
      </Router>
    )
  }
}

injectTapEventPlugin()

render(<App />, document.getElementById('app'))
