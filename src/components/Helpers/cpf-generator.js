/*eslint no-extend-native: ["off"]*/
String.prototype.repeat = String.prototype.repeat || function (n) {
  return n <= 1 ? this : (this + this.repeat(n - 1))
}

/*eslint no-extend-native: ["off"]*/
String.prototype.padLeft = function (char, length) {
  return char.repeat(Math.max(0, length - this.length)) + this
}

let random = (min, max, pad) => (~~(Math.random() * (max - min) + min))
  .toString()
  .padLeft((~~(Math.random() * (10))).toString(), pad)

class CPFMgmt {
  calculateDig (cpf9) {
    let len = cpf9.length + 1
    let numbers = cpf9.split('').map((n) => parseInt(n))
    let sum = ((arr) => {
      let r = 0
      for (var i = len; i >= 2; i--) {
        r += i * arr[len - i]
      }
      return r
    })(numbers)

    let mod = sum % 11
    let dig = mod < 2 ? 0 : 11 - mod

    return cpf9 + dig
  }

  format (cpf) {
    let arr = cpf.split('')
    arr.splice(3, 0, '.')
    arr.splice(7, 0, '.')
    arr.splice(11, 0, '-')
    return arr.join('')
  }

  CPFGenerate () {
    let cpf = ''.concat(random(1, 1000, 3), random(0, 1000, 3), random(0, 1000, 3))
    cpf = this.calculateDig(this.calculateDig(cpf))

    return {
      cpf: cpf,
      cpfCopied: false,
      cpfFormated: this.format(cpf),
      cpfFormatedCopied: false,
      isValid: true
    }
  }

  CPFValidate (cpf) {
    let _cpf = cpf.match(/\d+/g).join('')
    if (_cpf.length === 11) {
      let cpf9 = _cpf.substring(0, 9)
      let valid = this.calculateDig(this.calculateDig(cpf9)) === _cpf

      return {
        cpf: _cpf,
        cpfCopied: false,
        cpfFormated: this.format(_cpf),
        cpfFormatedCopied: false,
        isValid: valid
      }
    } else {
      return {
        cpf: _cpf,
        cpfCopied: false,
        cpfFormated: this.format(_cpf),
        cpfFormatedCopied: false,
        isValid: false
      }
    }
  }
}

export default CPFMgmt
