class Barcode {
  constructor () {
    this.i = 0
    this.ftemp = ''
    this.xPos = 0
    this.yPos = 0
    this.digits = 0

    this.canvas = document.createElement('canvas')
    this.canvasCtx = this.canvas.getContext('2d')
    this.canvasWidth = 0
    this.canvasHeight = 0
    this.cPattern = new Array(100)
    this.START = '0000'
    this.STOP = '1000'
  }

  _fillPattern () {
    let f
    let strTemp

    if (this.cPattern[0] === undefined) {
      this.cPattern[0] = '00110'
      this.cPattern[1] = '10001'
      this.cPattern[2] = '01001'
      this.cPattern[3] = '11000'
      this.cPattern[4] = '00101'
      this.cPattern[5] = '10100'
      this.cPattern[6] = '01100'
      this.cPattern[7] = '00011'
      this.cPattern[8] = '10010'
      this.cPattern[9] = '01010'
      //Create a draw pattern for each char from 0 to 99

      for (let f1 = 9; f1 >= 0; f1--) {
        for (let f2 = 9; f2 >= 0; f2--) {
          f = f1 * 10 + f2
          strTemp = ''
          for (let i = 0; i < 5; i++) {
            strTemp += this.cPattern[f1][i].toString() + this.cPattern[f2][i].toString()
          }
          this.cPattern[f] = strTemp
        }
      }
    }
  }

  generateBarcode (code) {
    this.digits = code.length

    if (this.digits % 2 > 0) this.digits++

    while (code.length < this.digits || code.length % 2 > 0) {
      code = '0' + code
    }

    //Full = width(1) * 3
    //Thin = width(1)

    let _width = (2 * 3 + 3 * 1) * (this.digits) + 7 * 1 + 3

    this.canvasWidth = _width
    this.canvasHeight = 50

    this._drawPattern(this.START)

    //Draw code
    this._fillPattern()
    while (code.length > 0) {
      this.i = parseInt(code.substring(0, 2), 10)
      if (code.length > 2) code = code.split('').splice(2).join('')
      else code = ''
      this._drawPattern(this.cPattern[this.i])
    }

    //Stop Patern
    this._drawPattern(this.STOP)

    return this.canvas
  }

  _drawPattern (pattern) {
    let tempWidth = 0
    let _thin = 1
    let _full = 3

    for (let i = 0; i < pattern.length; i++) {
      tempWidth = pattern[i] === 0
        ? _thin
        : _full

      this.canvasCtx.beginPath()
      this.canvasCtx.rect(this.xPos, this.yPos, tempWidth, this.canvasHeight)

      if (i % 2 === 0) {
        //g.FillRectangle(BLACK, xPos, yPos, tempWidth, _height);
        this.canvasCtx.fillStyle = 'black'
      } else {
        //g.FillRectangle(WHITE, xPos, yPos, tempWidth, _height);
        this.canvasCtx.fillStyle = 'white'
      }
      this.canvasCtx.fill()

      this.xPos += tempWidth
    }
  }
}

export default Barcode
