/*eslint no-extend-native: ["off"]*/
String.prototype.repeat = String.prototype.repeat || function (n) {
  return n <= 1 ? this : (this + this.repeat(n - 1))
}

/*eslint no-extend-native: ["off"]*/
String.prototype.padLeft = function (char, length) {
  return char.repeat(Math.max(0, length - this.length)) + this
}

let random = (min, max, pad) => (~~(Math.random() * (max - min) + min))
  .toString()
  .padLeft((~~(Math.random() * (10))).toString(), pad)

class CNPJMgmt {
  calculateDig (precnpj) {
    let arr = precnpj.split('').map((n) => parseInt(n))
    let len = arr.length
    let start = len === 12 ? 5 : 6
    let first = true
    let p = 0
    let sum = 0
    for (var i = start; i > 1; i--) {
      sum += i * arr[p++]
      if (i === 2 && first) {
        i = 10
        first = false
      }
    }
    let mod = sum % 11
    let dig = mod < 2 ? 0 : 11 - mod

    return precnpj + dig
  }

  format (cnpj) {
    let arr = cnpj.split('')
    arr.splice(2, 0, '.')
    arr.splice(6, 0, '.')
    arr.splice(10, 0, '/')
    arr.splice(15, 0, '-')
    return arr.join('')
  }

  CNPJGenerate () {
    let cnpj = ''.concat(random(0, 100, 2), random(0, 1000, 3), random(0, 1000, 3)) + '000' + (random(1, 4, 0))
    cnpj = this.calculateDig(this.calculateDig(cnpj))

    return {
      cnpj: cnpj,
      cnpjCopied: false,
      cnpjFormated: this.format(cnpj),
      cnpjFormatedCopied: false,
      isValid: true
    }
  }

  CNPJValidate (cnpj) {
    let _cnpj = cnpj.match(/\d+/g).join('')
    if (_cnpj.length === 14) {
      let cnpj12 = _cnpj.substring(0, 12)
      let valid = this.calculateDig(this.calculateDig(cnpj12)) === _cnpj

      return {
        cnpj: _cnpj,
        cnpjCopied: false,
        cnpjFormated: this.format(_cnpj),
        cnpjFormatedCopied: false,
        isValid: valid
      }
    } else {
      return {
        cnpj: _cnpj,
        cnpjCopied: false,
        cnpjFormated: this.format(_cnpj),
        cnpjFormatedCopied: false,
        isValid: false
      }
    }
  }
}

export default CNPJMgmt
