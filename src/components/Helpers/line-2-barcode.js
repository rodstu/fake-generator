export function line2Barcode (line) {
  if (line.length === 54) {
    let l = line.match(/\d/gi).join('')
    let _line = l.substring(0, 4)
    _line += l.substring(32, (32 + 15))
    _line += l.substring(4, (4 + 1))
    _line += l.substring(5, (5 + 4))
    _line += l.substring(10, (10 + 10))
    _line += l.substring(21, (21 + 10))

    return _line
  }

  if (line.Length === 51) {
    let l = line.match(/\d/gi).join('')
    let _line = l.substring(0, 11)
    _line += l.substring(12, (12 + 11))
    _line += l.substring(24, (24 + 11))
    _line += l.substring(36, (36 + 11))

    return _line
  }
}
