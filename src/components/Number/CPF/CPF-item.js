import React, { Component, PropTypes } from 'react'
import IconButton from 'material-ui/IconButton'
import IconCopy from 'material-ui/svg-icons/content/content-copy'
import classNames from 'classnames/bind'

import styles from './cpf-item.scss'

class CPFItem extends Component {
  constructor (props) {
    super(props)
    this.styleClass = classNames.bind(styles)
    this.state = {
      showUp: {
        cpfItem: true,
        show: false
      }
    }
  }

  componentDidMount () {
    this.setState({showUp: Object.assign(this.state.showUp, {
      show: true,
      cpfItem: true
    })})
  }

  render () {
    let {cpf, cpfCopied, cpfFormated, cpfFormatedCopied, isValid} = this.props.data
    let showFormated = this.props.showFormated
    let portrait = this.props.portrait

    if (!portrait) {
      return (
        <div className={this.styleClass(Object.assign({}, this.state.showUp, {showValid: isValid, showInvalid: !isValid}))}>
          <table className={this.styleClass()}>
            <tbody>
              <tr>
                <td />
                <td />
                <td className={this.styleClass({copiedF: cpfFormatedCopied})}>
                  <div className={styles.value}>{cpfFormated}</div>
                  <div className={styles.label}>copiado!</div>
                </td>
                <td>
                  <IconButton
                    onTouchTap={() => this.props.copy(this.props.data, true)}
                    tooltip='Copiar'>
                    <IconCopy />
                  </IconButton>
                </td>
                <td />
                <td className={this.styleClass({copied: cpfCopied})}>
                  <div className={styles.value}>{cpf}</div>
                  <div className={styles.label}>copiado!</div>
                </td>
                <td>
                  <IconButton
                    onTouchTap={() => this.props.copy(this.props.data, false)}
                    tooltip='Copiar'>
                    <IconCopy />
                  </IconButton>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      )
    } else {
      if (showFormated) {
        return (
          <div className={this.styleClass(Object.assign({}, this.state.showUp, {showValid: isValid, showInvalid: !isValid}))}>
            <table>
              <tbody>
                <tr>
                  <td />
                  <td />
                  <td className={this.styleClass({copiedF: cpfFormatedCopied})}>
                    <div className={styles.value}>{cpfFormated}</div>
                    <div className={styles.label}>copiado!</div>
                  </td>
                  <td>
                    <IconButton
                      onTouchTap={() => this.props.copy(this.props.data, true)}
                      tooltip='Copiar'>
                      <IconCopy />
                    </IconButton>
                  </td>
                  <td />
                  <td />
                  <td />
                </tr>
              </tbody>
            </table>
          </div>
        )
      } else {
        return (
          <div className={this.styleClass(Object.assign({}, this.state.showUp, {showValid: isValid, showInvalid: !isValid}))}>
            <table>
              <tbody>
                <tr>
                  <td />
                  <td />
                  <td className={this.styleClass({copied: cpfCopied})}>
                    <div className={styles.value}>{cpf}</div>
                    <div className={styles.label}>copiado!</div>
                  </td>
                  <td>
                    <IconButton
                      onTouchTap={() => this.props.copy(this.props.data, false)}
                      tooltip='Copiar'>
                      <IconCopy />
                    </IconButton>
                  </td>
                  <td />
                  <td />
                  <td />
                </tr>
              </tbody>
            </table>
          </div>
        )
      }
    }
  }
}

CPFItem.propTypes = {
  copy: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  portrait: PropTypes.bool.isRequired,
  showFormated: PropTypes.bool.isRequired
}

export default CPFItem
