import React, {Component} from 'react'
import {GerarCpf, SliderRange, CheckFormated, ValidaTxt, ClearTxt, ValidarCpf} from './cpf-components'
import {Tabs, Tab} from 'material-ui/Tabs'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import IconAdd from 'material-ui/svg-icons/content/add'
import CPFMgmt from './../../Helpers/cpf-generator'
import CopyClipboard from '../../Helpers/copy'
import CPFResult from './CPF-result'
import styles from './cpf.scss'

const CPFGen = new CPFMgmt()

class CPF extends Component {
  constructor (props) {
    super(props)

    this._handleQuantity = this.handleQuantity.bind(this)
    this._handleFormated = this.handleFormated.bind(this)
    this._handleQuantity = this.handleQuantity.bind(this)
    this._handleClear = this.handleClear.bind(this)
    this._validarCpf = this.validarCpf.bind(this)
    this._copiedCPF = this.copiedCPF.bind(this)

    this.state = {
      CPFItems: [CPFGen.CPFGenerate(), CPFGen.CPFGenerate(), CPFGen.CPFGenerate()],
      CPFValidateText: '',
      portrait: false,
      showFloatingAdd: true,
      showFormated: false,
      sliderValue: 3
    }
  }

  mediaQueryResponse = (mql) => {
    if (mql.matches) {
      this.setState({portrait: true})
    } else {
      this.setState({portrait: false})
    }
  }

  componentDidMount () {
    var mql = window.matchMedia('only screen and (orientation: portrait) and (max-width: 730px) and (max-device-width: 730px)')
    this.mediaQueryResponse(mql)
    mql.addListener(this.mediaQueryResponse)
  }

  handleClear = () => this.setState({ CPFValidateText: '', CPFItems: [] })

  handleFormated (e, checked) {
    this.setState({showFormated: checked})
  }

  handleTabActive (tab) {
    switch (tab) {
      case 'TAB_VALIDATE':
        this.setState({showFloatingAdd: false})
        break
      case 'TAB_GENERATE':
        this.setState({showFloatingAdd: true})
        break
    }
  }

  handleQuantity (e, value) {
    this.setState({sliderValue: value})
  }

  handleChange = (event) => {
    this.setState({
      CPFValidateText: event.target.value
    })
  }

  GenerateCPF () {
    var arr = []
    for (var i = 0; i < this.state.sliderValue; i++) {
      arr.push(CPFGen.CPFGenerate())
    }
    this.setState({CPFItems: arr})
  }

  copiedCPF (data, formated) {
    if (formated) {
      data.cpfFormatedCopied = !data.cpfFormatedCopied
      CopyClipboard(data.cpfFormated)
    } else {
      data.cpfCopied = !data.cpfCopied
      CopyClipboard(data.cpf)
    }
    let index = this.state.CPFItems.findIndex((e, i) => e.cpf === data.cpf)
    let arr = Object.assign([], this.state.CPFItems)
    arr.splice(index, 1, data)
    this.setState({ CPFItems: arr })
  }

  validarCpf () {
    let validated = this.state.CPFValidateText.split('\n').map(t => CPFGen.CPFValidate(t))
    this.setState({CPFItems: validated})
  }

  render () {
    let {CPFItems, CPFValidateText, portrait, showFloatingAdd, showFormated, sliderValue} = this.state
    return (
      <div className={styles.cpf}>
        <div>
          <Tabs>
            <Tab
              onActive={() => this.handleTabActive('TAB_GENERATE')}
              label='Gerar CPF' >
              <div className={styles.gerarCpf}>
                <div>
                  <GerarCpf onTouchTap={() => this.GenerateCPF()}
                  />
                </div>
                <div>
                  <SliderRange
                    defaultValue={sliderValue}
                    onChange={this._handleQuantity} />
                </div>
                <div>{sliderValue}</div>
              </div>
              {/* ========================== MOBILE ========================== */}
              <table className={styles.geraCpfMob}>
                <tbody>
                  <tr>
                    <td>
                      <CheckFormated checked={showFormated} onCheck={this._handleFormated} />
                    </td>
                    <td>
                      <SliderRange
                        defaultValue={sliderValue}
                        onChange={this._handleQuantity} />
                    </td>
                    <td>{sliderValue}</td>
                  </tr>
                  <tr>
                    <td colSpan='3'>
                      <GerarCpf onTouchTap={() => this.GenerateCPF()}
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
              {/* ========================== MOBILE ========================== */}
            </Tab>
            <Tab
              onActive={() => this.handleTabActive('TAB_VALIDATE')}
              label='Validar CPF' >
              <div className={styles.validarCpf}>
                <div>
                  <ValidaTxt
                    handleClear={this._handleClear}
                    value={CPFValidateText}
                    onChange={this.handleChange} />
                </div>
                <div>
                  <ValidarCpf
                    onTouchTap={this._validarCpf} />
                </div>
                <div>
                  <ClearTxt
                    handleClear={this._handleClear} />
                </div>
                <div />
              </div>
              {/* ========================== MOBILE ========================== */}
              <table className={styles.validarCpfMob}>
                <tbody>
                  <tr>
                    <td colSpan='2'>
                      <ValidaTxt
                        handleClear={this._handleClear}
                        value={CPFValidateText}
                        onChange={this.handleChange} />
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <ValidarCpf
                        onTouchTap={this._validarCpf} />
                    </td>
                    <td>
                      <ClearTxt
                        handleClear={this._handleClear} />
                    </td>
                  </tr>
                </tbody>
              </table>
              {/* ========================== MOBILE ========================== */}
            </Tab>
          </Tabs>
          {
            ((shouldShow) =>
              shouldShow
                ? <FloatingActionButton
                  onTouchTap={() => {
                    let bd = document.querySelector('body')
                    this.setState({CPFItems: [...CPFItems, CPFGen.CPFGenerate()]})
                    setTimeout(() => window.scrollTo(0, bd.scrollHeight), 100)
                  }}
                  className={styles.btnAdd}>
                  <IconAdd />
                </FloatingActionButton>
                : null
            )(showFloatingAdd)
          }
        </div>
        <CPFResult
          items={CPFItems}
          portrait={portrait}
          showFormated={showFormated}
          copy={this._copiedCPF} />
      </div>
    )
  }
}

export default CPF
