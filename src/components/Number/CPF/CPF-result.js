import React, {Component, PropTypes} from 'react'
import CPFItem from './CPF-item'
import styles from './cpf-result.scss'

export default class CPFResult extends Component {
  render () {
    return (
      <div className={styles.result}>
        {/* {
          [...Array(this.props.quantity)].map((e, i) => {
            return <CPFItem key={i} data={{formated: '111.222.333-99', numbers: '11122233399', isValid: true}} />
          })
        } */}
        {this.props.items.map((el, i) => {
          return <CPFItem
            key={i}
            data={el}
            portrait={this.props.portrait}
            showFormated={this.props.showFormated}
            copy={this.props.copy} />
        })}
      </div>
    )
  }
}

CPFResult.propTypes = {
  copy: PropTypes.func.isRequired,
  items: PropTypes.array.isRequired,
  portrait: PropTypes.bool.isRequired,
  showFormated: PropTypes.bool.isRequired
}
