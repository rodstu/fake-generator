import React, {Component} from 'react'
import {GerarCnpj, SliderRange, CheckFormated, ValidaTxt, ClearTxt, ValidarCnpj} from './cnpj-components'
import {Tabs, Tab} from 'material-ui/Tabs'
import FloatingActionButton from 'material-ui/FloatingActionButton'
import IconAdd from 'material-ui/svg-icons/content/add'
import CNPJMgmt from './../../Helpers/cnpj-generator'
import CopyClipboard from '../../Helpers/copy'
import CNPJResult from './CNPJ-result'
import styles from './cnpj.scss'

const CNPJGen = new CNPJMgmt()

class CNPJ extends Component {
  constructor (props) {
    super(props)

    this._handleQuantity = this.handleQuantity.bind(this)
    this._handleFormated = this.handleFormated.bind(this)
    this._handleQuantity = this.handleQuantity.bind(this)
    this._handleClear = this.handleClear.bind(this)
    this._validarCnpj = this.validarCnpj.bind(this)
    this._copiedCNPJ = this.copiedCNPJ.bind(this)

    this.state = {
      CNPJItems: [CNPJGen.CNPJGenerate(), CNPJGen.CNPJGenerate(), CNPJGen.CNPJGenerate()],
      CNPJValidateText: '',
      portrait: false,
      showFloatingAdd: true,
      showFormated: false,
      sliderValue: 3
    }
  }

  mediaQueryResponse = (mql) => {
    if (mql.matches) {
      this.setState({portrait: true})
    } else {
      this.setState({portrait: false})
    }
  }

  componentDidMount () {
    var mql = window.matchMedia('only screen and (orientation: portrait) and (max-width: 730px) and (max-device-width: 730px)')
    this.mediaQueryResponse(mql)
    mql.addListener(this.mediaQueryResponse)
  }

  handleClear = () => this.setState({ CNPJValidateText: '', CNPJItems: [] })

  handleFormated (e, checked) {
    this.setState({showFormated: checked})
  }

  handleTabActive (tab) {
    switch (tab) {
      case 'TAB_VALIDATE':
        this.setState({showFloatingAdd: false})
        break
      case 'TAB_GENERATE':
        this.setState({showFloatingAdd: true})
        break
    }
  }

  handleQuantity (e, value) {
    this.setState({sliderValue: value})
  }

  handleChange = (event) => {
    this.setState({
      CNPJValidateText: event.target.value
    })
  }

  GenerateCNPJ () {
    var arr = []
    for (var i = 0; i < this.state.sliderValue; i++) {
      arr.push(CNPJGen.CNPJGenerate())
    }
    this.setState({CNPJItems: arr})
  }

  copiedCNPJ (data, formated) {
    if (formated) {
      data.cnpjFormatedCopied = !data.cnpjFormatedCopied
      CopyClipboard(data.cnpjFormated)
    } else {
      data.cnpjCopied = !data.cnpjCopied
      CopyClipboard(data.cnpj)
    }
    let index = this.state.CNPJItems.findIndex((e, i) => e.cnpj === data.cnpj)
    let arr = Object.assign([], this.state.CNPJItems)
    arr.splice(index, 1, data)
    this.setState({ CNPJItems: arr })
  }

  validarCnpj () {
    let validated = this.state.CNPJValidateText.split('\n').map(t => CNPJGen.CNPJValidate(t))
    this.setState({CNPJItems: validated})
  }

  render () {
    let {CNPJItems, CNPJValidateText, portrait, showFloatingAdd, showFormated, sliderValue} = this.state
    return (
      <div className={styles.cnpj}>
        <div>
          <Tabs>
            <Tab
              onActive={() => this.handleTabActive('TAB_GENERATE')}
              label='Gerar CNPJ' >
              <div className={styles.gerarCnpj}>
                <div>
                  <GerarCnpj onTouchTap={() => this.GenerateCNPJ()}
                  />
                </div>
                <div>
                  <SliderRange
                    defaultValue={sliderValue}
                    onChange={this._handleQuantity} />
                </div>
                <div>{sliderValue}</div>
              </div>
              {/* ========================== MOBILE ========================== */}
              <table className={styles.geraCnpjMob}>
                <tbody>
                  <tr>
                    <td>
                      <CheckFormated checked={showFormated} onCheck={this._handleFormated} />
                    </td>
                    <td>
                      <SliderRange
                        defaultValue={sliderValue}
                        onChange={this._handleQuantity} />
                    </td>
                    <td>{sliderValue}</td>
                  </tr>
                  <tr>
                    <td colSpan='3'>
                      <GerarCnpj onTouchTap={() => this.GenerateCNPJ()}
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
              {/* ========================== MOBILE ========================== */}
            </Tab>
            <Tab
              onActive={() => this.handleTabActive('TAB_VALIDATE')}
              label='Validar CNPJ' >
              <div className={styles.validarCnpj}>
                <div>
                  <ValidaTxt
                    handleClear={this._handleClear}
                    value={CNPJValidateText}
                    onChange={this.handleChange} />
                </div>
                <div>
                  <ValidarCnpj
                    onTouchTap={this._validarCnpj} />
                </div>
                <div>
                  <ClearTxt
                    handleClear={this._handleClear} />
                </div>
                <div />
              </div>
              {/* ========================== MOBILE ========================== */}
              <table className={styles.validarCnpjMob}>
                <tbody>
                  <tr>
                    <td colSpan='2'>
                      <ValidaTxt
                        handleClear={this._handleClear}
                        value={CNPJValidateText}
                        onChange={this.handleChange} />
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <ValidarCnpj
                        onTouchTap={this._validarCnpj} />
                    </td>
                    <td>
                      <ClearTxt
                        handleClear={this._handleClear} />
                    </td>
                  </tr>
                </tbody>
              </table>
              {/* ========================== MOBILE ========================== */}
            </Tab>
          </Tabs>
          {
            ((shouldShow) =>
              shouldShow
                ? <FloatingActionButton
                  onTouchTap={() => {
                    let bd = document.querySelector('body')
                    this.setState({CNPJItems: [...CNPJItems, CNPJGen.CNPJGenerate()]})
                    setTimeout(() => window.scrollTo(0, bd.scrollHeight), 100)
                  }}
                  className={styles.btnAdd}>
                  <IconAdd />
                </FloatingActionButton>
                : null
            )(showFloatingAdd)
          }
        </div>
        <CNPJResult
          items={CNPJItems}
          portrait={portrait}
          showFormated={showFormated}
          copy={this._copiedCNPJ} />
      </div>
    )
  }
}

export default CNPJ
