import React, { Component, PropTypes } from 'react'
import IconButton from 'material-ui/IconButton'
import IconCopy from 'material-ui/svg-icons/content/content-copy'
import classNames from 'classnames/bind'

import styles from './cnpj-item.scss'

class CNPJItem extends Component {
  constructor (props) {
    super(props)
    this.styleClass = classNames.bind(styles)
    this.state = {
      showUp: {
        cnpjItem: true,
        show: false
      }
    }
  }

  componentDidMount () {
    this.setState({showUp: Object.assign(this.state.showUp, {
      show: true,
      cnpjItem: true
    })})
  }

  render () {
    let {cnpj, cnpjCopied, cnpjFormated, cnpjFormatedCopied, isValid} = this.props.data
    let showFormated = this.props.showFormated
    let portrait = this.props.portrait

    if (!portrait) {
      return (
        <div className={this.styleClass(Object.assign({}, this.state.showUp, {showValid: isValid, showInvalid: !isValid}))}>
          <table className={this.styleClass()}>
            <tbody>
              <tr>
                <td />
                <td />
                <td className={this.styleClass({copiedF: cnpjFormatedCopied})}>
                  <div className={styles.value}>{cnpjFormated}</div>
                  <div className={styles.label}>copiado!</div>
                </td>
                <td>
                  <IconButton
                    onTouchTap={() => this.props.copy(this.props.data, true)}
                    tooltip='Copiar'>
                    <IconCopy />
                  </IconButton>
                </td>
                <td />
                <td className={this.styleClass({copied: cnpjCopied})}>
                  <div className={styles.value}>{cnpj}</div>
                  <div className={styles.label}>copiado!</div>
                </td>
                <td>
                  <IconButton
                    onTouchTap={() => this.props.copy(this.props.data, false)}
                    tooltip='Copiar'>
                    <IconCopy />
                  </IconButton>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      )
    } else {
      if (showFormated) {
        return (
          <div className={this.styleClass(Object.assign({}, this.state.showUp, {showValid: isValid, showInvalid: !isValid}))}>
            <table>
              <tbody>
                <tr>
                  <td />
                  <td />
                  <td className={this.styleClass({copiedF: cnpjFormatedCopied})}>
                    <div className={styles.value}>{cnpjFormated}</div>
                    <div className={styles.label}>copiado!</div>
                  </td>
                  <td>
                    <IconButton
                      onTouchTap={() => this.props.copy(this.props.data, true)}
                      tooltip='Copiar'>
                      <IconCopy />
                    </IconButton>
                  </td>
                  <td />
                  <td />
                  <td />
                </tr>
              </tbody>
            </table>
          </div>
        )
      } else {
        return (
          <div className={this.styleClass(Object.assign({}, this.state.showUp, {showValid: isValid, showInvalid: !isValid}))}>
            <table>
              <tbody>
                <tr>
                  <td />
                  <td />
                  <td className={this.styleClass({copied: cnpjCopied})}>
                    <div className={styles.value}>{cnpj}</div>
                    <div className={styles.label}>copiado!</div>
                  </td>
                  <td>
                    <IconButton
                      onTouchTap={() => this.props.copy(this.props.data, false)}
                      tooltip='Copiar'>
                      <IconCopy />
                    </IconButton>
                  </td>
                  <td />
                  <td />
                  <td />
                </tr>
              </tbody>
            </table>
          </div>
        )
      }
    }
  }
}

CNPJItem.propTypes = {
  copy: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  portrait: PropTypes.bool.isRequired,
  showFormated: PropTypes.bool.isRequired
}

export default CNPJItem
