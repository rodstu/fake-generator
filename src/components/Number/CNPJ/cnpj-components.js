import React, {PropTypes} from 'react'
import IconClear from 'material-ui/svg-icons/content/clear'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'
import Slider from 'material-ui/Slider'
import Checkbox from 'material-ui/Checkbox'
import IconAdd from 'material-ui/svg-icons/content/add'
import IconCheck from 'material-ui/svg-icons/navigation/check'

export const GerarCnpj = (props) =>
  <RaisedButton
    label='Gerar'
    onTouchTap={props.onTouchTap}
    style={{width: '100%'}}
    primary
    icon={<IconAdd />} />

GerarCnpj.propTypes = {
  onTouchTap: PropTypes.func.isRequired
}

export const CheckFormated = (props) =>
  <Checkbox label='Formatado'
    labelStyle={{fontSize: '85%', marginLeft: '-8px', paddingLeft: '0'}}
    onCheck={props.onCheck}
    defaultChecked={props.checked} />

CheckFormated.propTypes = {
  onCheck: PropTypes.func.isRequired,
  checked: PropTypes.bool.isRequired
}

export const SliderRange = (props) =>
  <Slider
    sliderStyle={{marginTop: 5, marginBottom: 5}}
    min={1} max={20}
    step={1}
    value={props.defaultValue}
    defaultValue={props.defaultValue}
    onChange={props.onChange} />

SliderRange.propTypes = {
  defaultValue: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired
}

export const ValidaTxt = (props) =>
  <TextField
    hintText='Digite o CNPJ'
    multiLine
    fullWidth
    onChange={props.onChange}
    value={props.value}
    onKeyDown={(e) => {
      if (e.which === 27) { // esc
        props.handleClear()
      } else if (e.which === 13) { // enter
      }
    }} />

ValidaTxt.propTypes = {
  handleClear: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string
}

export const ValidarCnpj = (props) =>
  <RaisedButton
    label='Validar'
    primary
    fullWidth
    onTouchTap={props.onTouchTap}
    icon={<IconCheck />} />

ValidarCnpj.propTypes = {
  onTouchTap: PropTypes.func.isRequired
}

export const ClearTxt = (props) =>
  <FlatButton
    style={{marginLeft: '10px'}}
    primary
    onTouchTap={props.handleClear}
    icon={<IconClear />} />

ClearTxt.propTypes = {
  handleClear: PropTypes.func.isRequired
}
