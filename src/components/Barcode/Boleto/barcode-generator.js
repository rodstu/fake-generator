import React, {Component, PropTypes} from 'react'
import {line2Barcode} from './../../Helpers/line-2-barcode'

class BarcodeGenerator extends Component {
  setProperties () {
    this.i = 0
    this.xPos = 0
    this.yPos = 0
    this.digits = 0

    this.canvasHeight = 0
    this.cPattern = new Array(100)
    this.START = '0000'
    this.STOP = '1000'
  }

  fillPattern () {
    let f
    let strTemp

    if (this.cPattern[0] === undefined) {
      this.cPattern[0] = '00110'
      this.cPattern[1] = '10001'
      this.cPattern[2] = '01001'
      this.cPattern[3] = '11000'
      this.cPattern[4] = '00101'
      this.cPattern[5] = '10100'
      this.cPattern[6] = '01100'
      this.cPattern[7] = '00011'
      this.cPattern[8] = '10010'
      this.cPattern[9] = '01010'
      //Create a draw pattern for each char from 0 to 99

      for (let f1 = 9; f1 >= 0; f1--) {
        for (let f2 = 9; f2 >= 0; f2--) {
          f = f1 * 10 + f2
          strTemp = ''
          for (let i = 0; i < 5; i++) {
            strTemp += this.cPattern[f1][i].toString() + this.cPattern[f2][i].toString()
          }
          this.cPattern[f] = strTemp
        }
      }
    }
  }

  generateBarcode (code) {
    console.log(code)
    code = line2Barcode(code)
    console.log(code)

    this.digits = code.length

    if (this.digits % 2 > 0) this.digits++

    while (code.length < this.digits || code.length % 2 > 0) {
      code = '0' + code
    }

    let _width = (2 * 3 + 3 * 1) * (this.digits) + 7 * 1 + 3

    this.canvasHeight = 50
    this.canvas.height = this.canvasHeight
    this.canvas.width = _width
    this.canvas.style.zoom = 1

    this.drawPattern(this.START)

    this.fillPattern()
    while (code.length > 0) {
      this.i = parseInt(code.substring(0, 2), 10)
      if (code.length > 2) code = code.split('').splice(2).join('')
      else code = ''
      this.drawPattern(this.cPattern[this.i])
    }

    this.drawPattern(this.STOP)

    //this.drawText(this.props.text)
  }

  drawPattern (pattern) {
    let tempWidth = 0
    let _thin = 1
    let _full = 3

    for (let i = 0; i < pattern.length; i++) {
      this.canvasCtx.beginPath()
      tempWidth = pattern[i] === '0'
        ? _thin
        : _full

      this.canvasCtx.rect(this.xPos, this.yPos, tempWidth, this.canvasHeight)

      if (i % 2 === 0) {
        this.canvasCtx.fillStyle = '#000000' //'black'
      } else {
        this.canvasCtx.fillStyle = '#ffffff' //white
      }
      this.canvasCtx.fill()

      this.xPos += tempWidth
    }
  }

  drawText (text) {
    if (!text) return
    let _width = this.canvas.width
    let _height = this.canvas.height
    this.canvasCtx.beginPath()
    this.canvasCtx.fillStyle = 'red'
    this.canvasCtx.strokeStyle = 'white'
    this.canvasCtx.font = '900 16px roboto'
    let tsize = this.canvasCtx.measureText(text).width
    this.canvasCtx.fillText(text, _width / 2 - (tsize / 2), _height / 2 + (16 / 2))
    this.canvasCtx.strokeText(text, _width / 2 - (tsize / 2), _height / 2 + (16 / 2))
    this.canvasCtx.fill()
    this.canvasCtx.stroke()
  }

  componentDidMount () {
    this.setProperties()
    this.canvas = this.refs.canvas
    this.canvasCtx = this.canvas.getContext('2d')
    // console.log('componentDidMount', this.props.barcode)
    this.generateBarcode(this.props.barcode)
  }

  componentWillReceiveProps (nextProps) {
    this.setProperties()
    // console.log('componentWillReceiveProps', nextProps.barcode)
    this.generateBarcode(nextProps.barcode)
  }

  render () {
    return (
      <div>
        <canvas ref='canvas' />
      </div>
    )
  }
}

BarcodeGenerator.propTypes = {
  barcode: PropTypes.string.isRequired,
  text: PropTypes.string
}

export default BarcodeGenerator
