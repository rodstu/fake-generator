import BarcodeGenerator from './barcode-generator'
import styles from './boleto-item.scss'


import React, { Component } from 'react'

class BoletoItem extends Component {
  render () {
    return (
      <div>
        <table className={styles.tableHeader}>
          <tbody>
            <tr>
              <td><img src='../../../assets/boleto/237.png' /></td>
              <td>237-2</td>
              <td>23791.11103 60000.000103 01000.222206 1 48622000000000</td>
            </tr>
          </tbody>
        </table>

        <table cellSpacing='0' cellPadding='0' className={styles.tableBody}>
          <tbody>
            <tr>
              <td>
                <span>Local de pagamento</span>
                <h4>NÃO PAGUE, APENAS PARA TESTES</h4>
              </td>
              <td>
                <span>Vencimento</span>
                <h4>31/02/2015</h4>
              </td>
            </tr>
            <tr>
              <td>
                <span>Cedente</span>
                <h4>Banco X</h4>
              </td>
              <td>
                <span>Agência / Código cedente</span>
                <h4>XYZ</h4>
              </td>
            </tr>
            <tr>
              <td className={styles.tableIn1}>
                <div>
                  <span>Data documento</span>
                  <h4>25/01/2011</h4>
                </div>
                <div>
                  <span>Número documento</span>
                  <h4>NF 1 1 /1</h4>
                </div>
                <div>
                  <span>Espécie documento</span>
                  <h4>&nbsp;</h4>
                </div>
                <div>
                  <span>Aceite</span>
                  <h4>N</h4>
                </div>
                <div>
                  <span>Data processamento</span>
                  <h4>25/01/2011</h4>
                </div>
              </td>
              <td>
                <span>Carteira / Nosso número</span>
                <h4>06/00000001001-6</h4>
              </td>
            </tr>
            <tr>
              <td className={styles.tableIn2}>
                <div>
                  <span>Uso do banco</span>
                  <h4>&nbsp;</h4>
                </div>
                <div>
                  <span>Carteira</span>
                  <h4>06</h4>
                </div>
                <div>
                  <span>Espécie</span>
                  <h4>R$</h4>
                </div>
                <div>
                  <span>Quantidade</span>
                  <h4>&nbsp;</h4>
                </div>
                <div>
                  <span>(x) Valor</span>
                  <h4>&nbsp;</h4>
                </div>
              </td>
              <td>
                <span>(=) Valor documento</span>
                <h4>20.000.000,00</h4>
              </td>
            </tr>
            <tr>
              <td rowSpan='5' className={styles.tableIn3}>
                <div>
                  <span>Instruções (Texto de responsabilidade do cedente)</span>
                  <h4>Este boleto é gerado aleatoriamente, seu único propósito é testes de software.</h4>
                </div>
              </td>
              <td>
                <span>(-) Desconto / Abatimentos</span>
                <h4>&nbsp;</h4>
              </td>
            </tr>
            <tr>
              <td>
                <span>(-) Outras deduções</span>
                <h4>&nbsp;</h4>
              </td>
            </tr>
            <tr>
              <td>
                <span>(+) Mora / Multa</span>
                <h4>&nbsp;</h4>
              </td>
            </tr>
            <tr>
              <td>
                <span>(+) Outros acréscimos</span>
                <h4>&nbsp;</h4>
              </td>
            </tr>
            <tr>
              <td>
                <span>(=) Valor cobrado</span>
                <h4>&nbsp;</h4>
              </td>
            </tr>
            <tr className={styles.sacado}>
              <td>
                <span>Sacado</span>
                <h4>
                  <p>DISTRIBUIDORA DE AGUAS MINERAIS CNPJ: 00.000.000/0001-91</p>
                  <p>AV DAS FONTES 1777 10 ANDAR</p>
                  <p>PARQUE DAS FONTES - SAO PAULO/SP - CEP: 13950-000</p>
                </h4>
              </td>
              <td>&nbsp;</td>
            </tr>
            <tr className={styles.barcode}>
              <td>
                <span>Sacador / Avalista</span>
                <BarcodeGenerator
                  barcode='42297.11504 00001.954411 67191.969525 1 70090000225456'
                  //83690000000959200481000241234757100006602734
                  //3419175010114800100043354800026995000066105
                  //42297.11504 00001.954411 67191.969525 1 70090000225456
                  //42291700900002254567115000001954416719196952
                  //
                  />
              </td>
              <td>
                <span>Autencicação mecânica</span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
}

export default BoletoItem
