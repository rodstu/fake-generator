import React, { Component } from 'react'
import Toogle from 'material-ui/Toggle'
import DatePicker from 'material-ui/DatePicker'
import TextFiled from 'material-ui/TextField'
import styles from './boleto.scss'
import moment from 'moment'
import vmasker from 'vanilla-masker'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'
import IconClear from 'material-ui/svg-icons/content/clear'
import IconAdd from 'material-ui/svg-icons/content/add'
import areIntlLocalesSupported from 'intl-locales-supported'
import InputRange from 'react-input-range'

let DateTimeFormat

if (areIntlLocalesSupported(['pt', 'pt-BR'])) {
  DateTimeFormat = global.Intl.DateTimeFormat
} else {
  const IntlPolyfill = require('intl')
  DateTimeFormat = IntlPolyfill.DateTimeFormat
  require('intl/locale-data/jsonp/pt')
  require('intl/locale-data/jsonp/pt-BR')
}

class Boleto extends Component {
  constructor (props) {
    super(props)

    this._handleValuesChange = this.handleValuesChange.bind(this)

    this.state = {
      values: {
        min: 2,
        max: 10
      }
    }
  }

  handleValuesChange (component, values) {
    this.setState({
      values: values
    })
  }

  componentDidMount () {
    vmasker(document.querySelector('#text-total2pay')).maskMoney({
      precision: 2,
      separator: ',',
      delimiter: '.',
      unit: 'R$'
    })
  }

  render () {
    return (
      <div>
        <div>
          <table className={styles.tableControl}>
            <tbody>
              <tr>
                <td>
                  <Toogle
                    label='Automático'
                    thumbSwitchedStyle={{ backgroundColor: '#4d4de9' }}
                    defaultToggled
                    //trackSwitchedStyle={{backgroundColor: 'gray'}}
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <div>
                    <DatePicker
                      id='date-ini'
                      floatingLabelText='Vencimento'
                      DateTimeFormat={DateTimeFormat}
                      // okLabel='OK'
                      autoOk
                      //defaultDate={moment().add(15, 'days').toDate()}
                      cancelLabel='Cancelar'
                      locale='br'
                    />
                    <FlatButton
                      style={{marginLeft: '10px'}}
                      primary
                      //onTouchTap={props.handleClear}
                      icon={<IconClear />}
                    />
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <InputRange
                    maxValue={20}
                    minValue={0}
                    value={this.state.values}
                    onChange={this._handleValuesChange}
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <div>
                    <TextFiled
                      id='text-total2pay'
                      floatingLabelText='Valor'
                    />
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div>
                   slider
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <RaisedButton
                    label='Gerar'
                    //onTouchTap={props.onTouchTap}
                    style={{width: '100%'}}
                    primary
                    icon={<IconAdd />}
                  />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

export default Boleto
