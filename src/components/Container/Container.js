import React, { Component, PropTypes } from 'react'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import classNames from 'classnames/bind'

import CustomTheme from './Theme.js'
import { TransitionAnimation } from './Animation.js'
import NavMenu from './Nav-Menu'
import Header from './Header'
import styles from './container.scss'

let openCloseDrawer
let setnavBarActive
let styleClass = () => {}

export default class Container extends Component {
  static propTypes = {
    routes: PropTypes.array
  }

  constructor (props) {
    super(props)
    styleClass = classNames.bind(styles)
    this.state = {
      style: {
        marginOpen: true,
        marginClose: false
      }}
  }

  mediaQueryResponse = (mql) => {
    if (mql.matches) {
      openCloseDrawer(false, false)
      setnavBarActive(false)
      this.setState({ style: { marginClose: true, marginOpen: false } })
    } else {
      openCloseDrawer(true, true)
      setnavBarActive(true)
      this.setState({ style: { marginClose: false, marginOpen: true } })
    }
  }

  componentDidMount () {
    openCloseDrawer = this.refs.navbar.openCloseDrawer
    this.refs.header.resetSelection = this.refs.navbar.resetSelection
    this.refs.header.onMenuTap = this.refs.navbar.handleToggle
    setnavBarActive = this.refs.header.setnavBarActive

    var mql = window.matchMedia('only screen and (max-width: 960px), (max-device-width: 960px) and (orientation: portrait)')
    this.mediaQueryResponse(mql)
    mql.addListener(this.mediaQueryResponse)
  }

  render () {
    return (
      <MuiThemeProvider muiTheme={CustomTheme}>
        <div>
          <Header ref='header' />
          <NavMenu ref='navbar' />
          <div className={styleClass(this.state.style)}>
            {
              ((props) => {
                if (props.children) {
                  return (
                    <TransitionAnimation
                      path={props.location.pathname}>
                      {props.children}
                    </TransitionAnimation>
                  )
                } else {
                  return (
                    <TransitionAnimation
                      path={props.location.pathname}>
                      <div
                        className={styles.flexItem}>
                        {props.content}
                      </div>
                      <div
                        className={styles.flexItem}>
                        {props.context}
                      </div>
                    </TransitionAnimation>
                  )
                }
              })(this.props)
            }
          </div>
        </div>
      </MuiThemeProvider>
    )
  }
}
