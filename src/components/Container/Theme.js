import getMuiTheme from 'material-ui/styles/getMuiTheme'
import {indigo500, indigo700, grey400, grey500, darkBlack, white, grey300,
  fullBlack} from 'material-ui/styles/colors'

export default getMuiTheme({
  palette: {
    primary1Color: '#4f4f4f',
    primary2Color: indigo700,
    primary3Color: grey400,
    accent1Color: '#ff4778',
    accent2Color: '#4f4f4f', // grey100,
    accent3Color: grey500,
    textColor: darkBlack,
    alternateTextColor: white,
    canvasColor: white,
    borderColor: grey300,
    // disabledColor: fade(darkBlack, 0.3),
    pickerHeaderColor: indigo500,
    // clockCircleColor: fade(darkBlack, 0.07),
    shadowColor: fullBlack
  }
})
