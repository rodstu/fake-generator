import React, {PropTypes} from 'react'
import EasyTransition from 'react-easy-transition'

import styles from './container.scss'

export const TransitionAnimation = (props) => {
  return (
    <EasyTransition
      path={props.path}
      initialStyle={{ opacity: 0, marginTop: '100px' }}
      transition='opacity 0.3s ease-in, margin-top 0.3s ease-in'
      finalStyle={{ opacity: 1, marginTop: '60px' }}
      leaveStyle={{ opacity: 0, marginTop: '0' }}
    >
      <div
        className={styles.flexContainer}>
        {props.children}
      </div>
    </EasyTransition>
  )
}

TransitionAnimation.propTypes = {
  path: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired
}
