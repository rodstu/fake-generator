import React, { Component, PropTypes } from 'react'
import Drawer from 'material-ui/Drawer'
import Subheader from 'material-ui/Subheader'
import { List, ListItem, makeSelectable } from 'material-ui/List'
import hashHistory from 'react-router/lib/hashHistory'
import { IconBarcode, IconId, IconId2 } from './Icons'

let SelectableList = makeSelectable(List)

function wrapState (ComposedComponent) {
  return class SelectableList extends Component {
    static propTypes = {
      children: PropTypes.node.isRequired,
      defaultValue: PropTypes.number.isRequired
    };

    componentWillMount () {
      this.setState({
        selectedIndex: this.props.defaultValue
      })
    }

    resetSelection = () => {
      this.setState({
        selectedIndex: -1
      })
    }

    handleRequestChange = (event, index) => {
      this.setState({
        selectedIndex: index
      })
    };

    render () {
      return (
        <ComposedComponent
          value={this.state.selectedIndex}
          onChange={this.handleRequestChange}
        >
          {this.props.children}
        </ComposedComponent>
      )
    }
  }
}

SelectableList = wrapState(SelectableList)

class NavMenu extends Component {
  constructor (props) {
    super(props)
    this.state = { open: false, docked: true }
  }

  handleToggle = () => {
    if (!this.state.docked) {
      this.setState(Object.assign(this.state, { open: !this.state.open }))
    }
  }

  openCloseDrawer = (show, dock) => {
    this.setState({open: show, docked: dock})
  }

  resetSelection = () => {
    this.refs.listMenu.resetSelection()
  }

  render () {
    return (
      <Drawer
        containerStyle={{
          boxShadow: '5px 0px 7px -5px rgba(0,0,0,0.75)',
          background: 'linear-gradient(to bottom, rgba(255,255,255,0.9) 0%, rgba(255,255,255,0.98) 76%, rgba(159,183,212,1) 100%)'}}
        swipeAreaWidth={50}
        zDepth={0}
        docked={this.state.docked}
        width={270}
        disableSwipeToOpen={false}
        open={this.state.open}
        onRequestChange={(open) => this.setState({open})}>
        <SelectableList ref='listMenu' defaultValue={0}>
          <Subheader>Selecione um item</Subheader>
          <ListItem
            value={1}
            primaryText='Código Numérico'
            initiallyOpen
            primaryTogglesNestedList
            nestedItems={[
              <ListItem
                value={2}
                primaryText='CNPJ'
                onTouchTap={() => {
                  hashHistory.push('/numerico/cnpj')
                  this.handleToggle()
                }}
                leftIcon={<IconId2 viewBox='0 0 442 442' />}
              />,
              <ListItem
                value={3}
                primaryText='CPF'
                onTouchTap={() => {
                  hashHistory.push('/numerico/cpf')
                  this.handleToggle()
                }}
                leftIcon={<IconId viewBox='0 0 60 60' />}
              />
            ]} />
          <ListItem
            value={4}
            primaryText='Código de Barra'
            initiallyOpen
            primaryTogglesNestedList
            nestedItems={[
              <ListItem
                value={6}
                primaryText='Água, Luz, Telefone'
                onTouchTap={() => {
                  hashHistory.push('/codigobarra/contaconsumo')
                  this.handleToggle()
                }}
                leftIcon={<IconBarcode />}
              />,
              <ListItem
                value={5}
                primaryText='Boleto Bancário'
                onTouchTap={() => {
                  hashHistory.push('/codigobarra/boleto')
                  this.handleToggle()
                }}
                leftIcon={<IconBarcode />}
              />
            ]} />
        </SelectableList>
      </Drawer>
    )
  }
}

export default NavMenu
