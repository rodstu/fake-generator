import React, { Component, PropTypes } from 'react'
import Icon from 'react-fa'
import classNames from 'classnames/bind'
import styles from './header.scss'
import Link from 'react-router/lib/Link'

let styleClass = () => {}

const Autor = (props) => {
  let name = props.isOpen
    ? 'Rodrigo Stuchi'
    : 'RodStu'
  return (<h5><span>by</span> {name}</h5>)
}
Autor.propTypes = {
  isOpen: PropTypes.bool.isRequired
}

export default class Header extends Component {
  constructor (props) {
    super(props)
    styleClass = classNames.bind(styles)
    this.state = {
      icoStyle: {
        item: true,
        menuOpen: true,
        menuClose: false
      }}
  }

  setnavBarActive = (navBarActive) => {
    let ico = Object.assign(this.state.icoStyle, {
      menuOpen: navBarActive,
      menuClose: !navBarActive
    })

    this.setState({
      icoStyle: ico
    })
  }

  onMenuTap () {}
  resetSelection () {}

  render () {
    return (
      <div className={styles.header}>
        <div
          className={styleClass(this.state.icoStyle)} >
          <Icon
            name='bars'
            fixedWidth
            size='lg'
            className={styles.hambuerger}
            onTouchTap={() => this.onMenuTap()}
            />
        </div>
        <div className={styles.item}>
          <h1 onClick={() => this.resetSelection()}>
            <Link to='/'>Fake Generator</Link>
          </h1>
        </div>
        <div className={styles.item}>
          <Autor isOpen={this.state.icoStyle.menuOpen} />
        </div>
      </div>
    )
  }
}
