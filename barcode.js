//from C# ToInt32

//01/12/2016
var a = moment([2016, 11, 1]).utc()

//01/01/1970
var b = moment([1970, 0, 1]).utc()

Math.round(a.diff(b, 'days', true) + 1)
//17137

//from C# FromInt32ToDateTime
//17137 + 10 => 11/12/2016 22:00:00

moment([1970, 0, 1]).utc().add(17136 + 10, 'days')
//moment("2016-12-11T00:00:00.000")